import React, { Component } from 'react';
import {
  Col, Button, DropdownButton, MenuItem,
  Alert, Image,
  Modal, FormControl,
} from 'react-bootstrap';
import { browserHistory, Link, } from 'react-router';
import firebase from 'firebase';
import slug from 'slug';
import moment from 'moment';
import ReactQuill from "react-quill";
import Dropzone from 'react-dropzone';

/** COMPONENTS CUSTOM **/
import { APP_CONFIG } from './../../../boot.config';
import { TOOLS } from './../../../util/tools.global';
import { MasterForm } from './../../Global/Form/MasterForm';
// import { Input } from '../Form/Input';
export class Page extends Component{
  constructor(props){
    super(props);
    this.state = {
      appID: document.querySelector('[data-js="root"]').dataset.appid || null,

      isLoading : true,
      showModal : false,
      confirmDelete : false,
      action : this.props.action,
      pages : [],
      categories : [],
      filter: 'Filtrar por',
      filterCategory: 'Selecione uma categoria',

      uid : this.props.currentUser.uid,
      pageID : null,
      title : null,
      slug : null,
      content : '',
      typeContent : 'Page',
      category : false,
      status : true,
      createAt: TOOLS.getDate(),
      updateAt: TOOLS.getDate(),

      files: [],
      photos: [],
      thumbnail: [],

      status_response : null,
      message_response : null
    }

    this.checkStatusResponse = this.checkStatusResponse.bind(this);
    this.checkStates = this.checkStates.bind(this);
    this.getPages = this.getPages.bind(this);
    this.resetCurrentState = this.resetCurrentState.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.requiredInput = TOOLS.requiredInput.bind(this);

    /*** FUNCTIONS AUXILIARES DO FORM ***/
    this.onLoadForm = this.onLoadForm.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onUpdateForm = this.onUpdateForm.bind(this);
    this.onChangeForm = this.onChangeForm.bind(this);
    this._onChangeTitle = this._onChangeTitle.bind(this);
    this.onChangeContent = this.onChangeContent.bind(this);
    this.renderType = this.renderType.bind(this);
    this.onSelectCategory = this.onSelectCategory.bind(this);
    this.onSelectTypeContent = this.onSelectTypeContent.bind(this);
    this.onSelectFilter = this.onSelectFilter.bind(this);
    this.onSelectFilterCategory = this.onSelectFilterCategory.bind(this);
    this.onSelectFilterCategoryList = this.onSelectFilterCategoryList.bind(this);
    this.statusInfo = this.statusInfo.bind(this);

    this.onDropFile = this.onDropFile.bind(this);
    this.onDropFileClick = this.onDropFileClick.bind(this);
    this.runUploadFiles = this.runUploadFiles.bind(this);

    /*** RENDER FUNCTIONS - ACTIONS ***/
    this.actionNull = this.actionNull.bind(this);
    this.actionNew = this.actionNew.bind(this);
    this.actionDelete = this.actionDelete.bind(this);
    this.renderAction = this.renderAction.bind(this);
  }

  componentWillMount(){
    // this.getPages();
    console.log('componentWillMount');
    if(this.props.action === 'edit' && this.props.pageID){
      console.log('componentWillMount if edit ')
      this.setState({ pageID : this.props.pageID });
    }
  }

  componentDidMount(){
    console.log('User id -> ',this.state.uid);
    let _self=this;
    _self.allPages = firebase.database().ref('apps/'+ this.state.appID +'/pages/');
    _self.allCategories = firebase.database().ref('apps/'+ this.state.appID +'/categories/');
    _self.pages = this.state.pages;
    _self.categories = this.state.categories;

    /** child_added **/
    _self.allPages.on('child_added', (data) => {
      _self.pages = this.state.pages;
      _self.pages.push(data.val());
      if(data){
        if(_self.pages.length >= 0 ){
          _self.setState({ pages : _self.pages, isLoading : false });
        }
      }
    });
    _self.allCategories.on('child_added', (data) => {
      _self.categories.push(data.val());
      if(data){
        if(_self.categories.length > 0 )
        _self.setState({ categories : _self.categories, isLoading : false });
      }
    });
    /** child_added **/

    /** child_changed **/
    _self.allPages.on('child_changed', (data) => {
      if(data){
        let changed = data.val();
        if(_self.pages.length > 0 ){
          console.log('child_changed pages: ', {changed : changed});
          // eslint-disable-next-line
          _self.pages.map((page) => {
            if(page.pageID === changed.pageID){
              page.uid = this.state.uid;
              page.title = changed.title;
              page.slug = changed.slug;
              page.content = changed.content;
              page.category = changed.category;
              page.typeContent = changed.typeContent;
              page.thumbnail = changed.thumbnail;
              page.status = changed.status;
              page.createAt = changed.createAt;
              page.updateAt = changed.updateAt;
            }
          });
          _self.setState({ pages : _self.pages, isLoading : false });
        }else{
          return false;
        }
      }
    });
    _self.allCategories.on('child_changed', (data) => {
      if(data){
        let changed = data.val();
        if(_self.categories.length > 0 ){
          console.log('child_changed categories: ', {changed : changed});
          // eslint-disable-next-line
          _self.categories.map((category) => {
            if(category.categoryID === changed.categoryID){
              category.uid = this.state.uid;
              category.title = changed.title;
              category.description = changed.description;
              category.status = changed.status;
              category.createAt = changed.createAt;
              category.updateAt = changed.updateAt;
            }
          });
          _self.setState({ categories : _self.categories, isLoading : false });
        }
      }
    });
    /** child_changed **/

    /** child_moved **/
    _self.allPages.on('child_moved', (data) => {
      _self.pages.push(data.val());
      if(data){
        if(_self.pages.length > 0 )
          _self.setState({ isLoading : false });

        _self.setState({ pages : _self.pages });
      }
      console.log('child_moved pages: ', {qtd: _self.pages.length, all: _self.pages});
    });
    _self.allCategories.on('child_moved', (data) => {
      _self.categories.push(data.val());
      if(data){
        if(_self.categories.length > 0 )
          _self.setState({ categories : _self.categories, isLoading : false });
      }
      console.log('child_moved categories: ', {qtd: _self.categories.length, all: _self.categories});
    });
    /** child_moved **/

    /** child_removed **/
    _self.allPages.on('child_removed', (data) => {
      if(data){
        if(_self.pages.length > 0 ){
          let pageRemoved = data.val();
          console.log('res child_removed -> ',pageRemoved);
          // eslint-disable-next-line
          _self.pages.map((page, key) => {
            // eslint-disable-next-line
            if(page.pageID === pageRemoved.pageID){
              console.log('Key -> ',key);
              TOOLS.removeArrayItem(_self.pages,key,1);
              console.log('Page removeed ',pageRemoved.pageID);
              _self.setState({ pages : _self.pages, isLoading : false });
            }else{
              console.log('Key -> ',key);
              console.log('Page pageID ',page.pageID);
              console.log('Page removeed ',pageRemoved.pageID);
            }
          });
        }
      }
    });
    _self.allCategories.on('child_removed', (data) => {
      if(data){
        if(_self.categories.length > 0 ){
          let categoryRemoved = data.val();
          console.log('res child_removed -> ',categoryRemoved);
          // eslint-disable-next-line
          _self.categories.map((category, key) => {
            // eslint-disable-next-line
            if(category.categoryID === categoryRemoved.categoryID){
              console.log('Key -> ',key);
              TOOLS.removeArrayItem(key,1);
              console.log('Category removeed ',categoryRemoved.categoryID);
              _self.setState({ categories : _self.categories, isLoading : false });
            }else{
              console.log('Key -> ',key);
              console.log('Category categoryID ',category.categoryID);
              console.log('Category removeed ',categoryRemoved.categoryID);
            }
          });
        }
      }
    });
    /** child_removed **/
  };//componentDidMount();

  componentWillUnmount(){
    console.clear();
  };//componentWillUnmount();

  componentWillReceiveProps(nextProps){
    console.log('componentWillReceiveProps ',nextProps);

    if(nextProps.action === 'new'){
      return this.resetCurrentState();
    }else if(nextProps.categoryID || nextProps.action === 'edit'){
      console.log('getPages to ',nextProps.action);
      console.log('getPages to ',nextProps);
      return this.getPages(nextProps.pageID);
    }
  };//componentWillReceiveProps();

  checkStatusResponse(){
    const closeAlert = (e) => {
      if(e)
        e.preventDefault();
      this.setState({ status_response : null, message_response : null});
    };//closeAlert(e);
    if(this.state.status_response === true){
      setTimeout(() => {
        closeAlert();
      },4000);
      return (<Alert bsStyle={'success'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    if(this.state.status_response === false){
      setTimeout(() => {
        closeAlert();
      },4000);
      if(this.state.status === 'Rascunho'){
        return (<Alert className={'default'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }else if(this.state.status === 'requiredInput'){
        return (<Alert className={'warning'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }
      return (<Alert bsStyle={'danger'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    return null;
  };//checkStatusResponse();

  checkStates(){
    if(this.state.pageID)
      this.setState({ pageID : this.state.pageID })

    console.log('checkStates exit();');
  };//checkStates();

  getPages(pageID){
    // console.log('Init getPages ->',pageID);
    if( pageID ){
      // console.log('Getting pageID ->',pageID);
      // eslint-disable-next-line
      return this.state.pages.map((page) => {
        // eslint-disable-next-line
        if(page.pageID === pageID){
          this.setState({
            pageID : page.pageID,
            title : page.title,
            slug : page.slug,
            content : page.content,
            photos : page.photos,
            thumbnail: page.thumbnail,
            typeContent : page.typeContent,
            category : page.category,
            status: page.status
          });
        }else{
          return false;
        }
      });
      // firebase.database().ref('pages/'+pageID).once('value')
      //   .then((res) => {
      //     console.log('getPage 1 res ',res.val().name);
      //   })
      //   .catch((err) => {
      //     console.log('getPage 1 err',err);
      //   })
    }
    this.setState({ isLoading : false });
  };//getPages();

  resetCurrentState(){
    console.log('resetCurrentState();');
    return this.setState({
      showModal : false,
      confirmDelete : false,
      action : this.props.action,

      uid : this.props.currentUser.uid,
      pageID : null,
      title : null,
      slug : null,
      content : null,
      photos : null,
      thumbnail : null,
      typeContent : 'Page',
      category : false,
      status : true,
      createAt : TOOLS.getDate(),
      updateAt : TOOLS.getDate(),

      status_response : null,
      message_response : null
    });
  };//resetCurrentState();

  renderModal(){
    let
      pageTitle = this.state.title;
    const close = () => {
      this.setState({ showModal : false });
    }
    const confirm = () => {
      firebase.database().ref('apps/'+ this.state.appID +'/pages/'+this.state.pageID)
        .remove()
        .then(() => {
          this.setState({
            isLoading : false,
            showModal : false,
            confirmDelete : false,
            status : 'Deletado',
            status_response : true,
            message_response : '{ ' + this.state.title + ' } deletado com sucesso!'
          });
        })
        .catch((err) => {
          this.setState({
            isLoading : false,
            showModal : false,
            confirmDelete : true,
            status : 'Error',
            status_response : false,
            message_response : 'Ocorreu uma falha ao tentar deletar { ' + this.state.title + ' } - '+err.message
          });
        });
        console.clear();
    }
    return (
      <Modal show={this.state.showModal} onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title>Deseja realmente excluir {pageTitle}?</Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Link to={'/dashboard/pages'} onClick={close.bind(this)} className={'btn btn-danger'}>Cancelar</Link>
          <Button onClick={confirm.bind(this)} bsStyle={'success'}>Confirmar</Button>
        </Modal.Footer>
      </Modal>
    );
  };//renderModal();

  /*** FUNCTIONS AUXILIARES DO FORM ***/
  onLoadForm(){
    console.log('onLoadForm();');
  };//onLoadForm(e);
  onSubmitForm(e) {
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const pageID = this.refs.pageID.value;
      const dataInsert = {
        uid : this.state.uid,
        pageID : pageID,
        title : this.state.title,
        slug : this.state.slug.toLowerCase(),
        content : this.state.content,
        photos : this.state.photos,
        thumbnail : this.state.thumbnail,
        typeContent : this.state.typeContent,
        category : this.state.category,
        status: 'Publicado',
        createAt: TOOLS.getDate(),
        updateAt: TOOLS.getDate()
      };
      firebase.database().ref('apps/'+ this.state.appID +'/pages/'+pageID)
        .set(dataInsert, () => {
          this.setState({
            isLoading : false,
            status_response : true,
            message_response : '{ ' + this.state.title + ' } gravado com sucesso!'
          });
          console.clear();
          browserHistory.push('/dashboard/pages');
        });
    }
    return false;
  };//onSubmitForm(e);
  onUpdateForm(e){
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const pageID = this.state.pageID;
      const dataUpdate = {
        uid : this.state.uid,
        pageID : pageID,
        title : this.state.title,
        slug : this.state.slug.toLowerCase(),
        content : this.state.content,
        category : this.state.category || false,
        photos : this.state.photos,
        thumbnail : this.state.thumbnail,
        typeContent : this.state.typeContent,
        status: this.state.status,
        createAt: this.state.createAt,
        updateAt: TOOLS.getDate(),
      };
      console.log('dataUpdate -> ',dataUpdate);
      firebase.database().ref('apps/'+ this.state.appID +'/pages/'+pageID)
        .update(dataUpdate, () => {
          this.setState({
            isLoading : false,
            pageID : pageID,
            content: '',
            status : 'Publicado',
            status_response : true,
            message_response : '{ ' + this.state.title + ' } gravado com sucesso!'
          });
          console.clear();
          browserHistory.push('/dashboard/pages');
        });
    }
    return false;
  };//onUpdateForm(e);
  onChangeForm(e){
    console.log('onChangeForm',e)
    if(e){
      // eslint-disable-next-line
      this.state.pages.map((page) => {
        if(page.slug === slug(e.target.value)){
          this.setState({
            pageID : this.refs.pageID.value,
            title : this.refs.title.value,
            slug: slug(this.refs.title.value)+'_2',
            content : this.refs.content.value,
            typeContent: this.state.typeContent,
            category : this.state.category,
            photos : this.state.photos,
            thumbnail : this.state.thumbnail,
            status: this.state.status,
            createAt: this.state.createAt,
            updateAt: TOOLS.getDate()
          });
        }else{
          this.setState({
            pageID : this.refs.pageID.value,
            title : this.refs.title.value,
            slug: slug(this.refs.title.value),
            content : this.refs.content.value,
            typeContent: this.state.typeContent,
            category : this.state.category,
            photos : this.state.photos,
            thumbnail : this.state.thumbnail,
            status: this.state.status,
            createAt: this.state.createAt,
            updateAt: TOOLS.getDate()
          });
        }
      });
      console.clear();
    }
  };//onChangeForm(e);
  _onChangeTitle(e){
    if(e){
      this.setState({
        title: e.target.value,
        slug: slug(e.target.value),
        status: 'Publicado',
        createAt: this.state.createAt,
        updateAt: TOOLS.getDate(),
      });
    }
  };//onChangeInput(e);
  onChangeContent(value){
      this.setState({ content : value });
  };//onChangeContent(e);
  renderType(){
    if(this.state.typeContent === 'Post'){
      const renderDropdownCategories = () => {
        if(this.state.categories.length === 0){

        }else{
          return this.state.categories.map((category) => {
            return (
              <MenuItem key={category.categoryID} className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectCategory}>{category.title}</MenuItem>
            );
          });
        }
      }
      return (
        <div>
          <label>Tipo de Conteúdo</label>
          <DropdownButton id={'typeContent'} className={'col-xs-12 col-md-12'} title={this.state.typeContent}>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectTypeContent}>Page</MenuItem>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectTypeContent}>Post</MenuItem>
          </DropdownButton>
          { (this.state.categories.length > 0) ?
            <DropdownButton id={'categoryList'} className={'col-xs-12 col-md-12'} style={{marginTop:20}} title={this.state.category}>
              {renderDropdownCategories()}
            </DropdownButton>
          :
          <Link to={'/dashboard/categories/new'} className={'categoryList btn-sm btn-warning text-center no-radius col-xs-12 col-md-12'} style={{marginTop:20}} title={'clique e cadastre uma categoria'}>Nenhuma categoria cadastrada</Link>
          }

          <label style={{marginTop:10}}>Imagem destaque</label>
          {this.state.thumbnail.length > 0 && !this.state.isLoading && this.state.thumbnail ?
            <div>
              <figure>
                <Button bsStyle={'danger'} bsSize={'small'} className={'removeThumbnail'}><i className={'fa fa-times'}></i></Button>
                <Image src={this.state.thumbnail} responsive />
              </figure>
            </div>
          :
            <div>
            {!this.state.thumbnail && !this.state.isLoading ?
              <div>
                <Dropzone ref="dropzone" style={{margin:0}} className={'dropzone'} activeClassName={'dragIn'} onDrop={this.onDropFile} >
                  <span>Arraste a imagem destaque para cá, ou clique para selecionar a imagem.</span>
                </Dropzone>
                <button type="button" onClick={this.onDropFileClick} style={{display:'none'}}>
                  OpenBox
                </button>
              </div>
            :
              <div>
              { this.state.isLoading ?
                  <div className={'spinner2'}>
                    <span className={'double-bounce1'}></span>
                    <span className={'double-bounce2'}></span>
                  </div>
              :
                // eslint-disable-next-line
                this.state.thumbnail.map((url,key) => {
                  if(!this.state.isLoading && this.state.thumbnail){
                    return (<figure key={key}><Image src={url} responsive /></figure>)
                  }else{
                    return (<figure key={key}>Carregando..</figure>)
                  }
                })
              }
              </div>
            }
            </div>
          }
        </div>
      );
    }else{
      return (
        <div>
          <label>Tipo de Conteúdo</label>
          <DropdownButton id={'typeContent'} className={'col-xs-12 col-md-12'} title={this.state.typeContent}>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectTypeContent}>Page</MenuItem>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectTypeContent}>Post</MenuItem>
          </DropdownButton>
        </div>
      );
    }
  };//renderType();
  onSelectCategory(e){
    if(e && e.target.innerHTML !== '')
      this.setState({ category : e.target.innerHTML });
  };//onSelectCategory();
  onSelectTypeContent(e){
    if(e && e.target.innerHTML === 'Page')
      this.setState({ typeContent : e.target.innerHTML, category : false });

    if(e && e.target.innerHTML === 'Post')
      this.setState({ typeContent : e.target.innerHTML, category : 'Selecione a Categoria' });
  };//onSelectTypeContent(e);
  onSelectFilter(e){
    if(e && e.target.innerHTML !== '')
      this.setState({ filter : e.target.innerHTML });
  }//onSelectFilter(e);
  onSelectFilterCategory(e){
    if(e && e.target.innerHTML !== '')
      this.setState({ filterCategory : e.target.innerHTML });
  }//onSelectFilterCategory(e);
  onSelectFilterCategoryList(){
    if(this.state.categories && this.state.categories.length > 0){
      const categorylist = () => {
        return this.state.categories.map((category) => {
          return (
            <MenuItem key={category.categoryID} onClick={this.onSelectFilterCategory}>{category.title}</MenuItem>
          );
        });
      }

      return (
        <DropdownButton
          bsStyle={'default'}
          title={this.state.filterCategory}
          id={'filterCategory'}
          className={'no-radius'}>
            {categorylist()}
        </DropdownButton>
      );
    }
  }//onSelectFilterCategoryList();
  statusInfo(){
    if( this.state.status ){
      if(this.state.status === 'Publicado')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-success' }>{'Publicado'}</span>
          </label>
        );

      if(this.state.status === 'Rascunho')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-default' }>{this.state.status || 'Aguardando publicação..'}</span>
          </label>
        );

      if(this.state.status === 'requiredInput')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-default' }>{'Rascunho'}</span>
          </label>
        );
    }else{
      return (
        <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
          <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-default' }>{'Aguardando um título'}</span>
        </label>
      );
    }
  };//statusInfo();

  onDropFile(files){
    console.log('Received files: ', files);
    this.setState({files:files});
    this.runUploadFiles(files);
  };//onDropFile(files);

  onDropFileClick(){
    console.log('this.refs.dropzone' , this.refs.dropzone)
    this.refs.dropzone.open();
  };//onDropFileClick();

  runUploadFiles(files){
    console.log('runUploadFiles');
    let storageRef = firebase.storage().ref('apps/'+ this.state.appID +'/uploads/'+this.state.pageID+'/');

    if(files.length > 0){
      // eslint-disable-next-line
      files.map((file,key) => {
        let fileID = TOOLS.uniqueID(),
            fileName_old = file.name,
            fileName = fileID + '_' + fileName_old,
            fileRef = storageRef.child('images/' + fileName),
            // pathFile = fileRef.fullPath,
            // fileSize = file.size,
            // fileType = file.type,
            uploadTask = fileRef.put(file);
            this.setState({isLoading : true});

        uploadTask.on('state_changed', (snapshot) => {
          console.log('snapshot -> ',snapshot);
        }, (error) => {
          console.log('error -> ',error);
        }, () => {
          fileRef.getDownloadURL().then((url) => {
            console.log('URL -> ',url);
            // let collectionPhotos = this.state.photos;
            // collectionPhotos.push(url);
            const pageID = this.state.pageID;
            const dataUpdate = {
              uid : this.state.uid,
              pageID : pageID,
              title : this.state.title,
              slug : this.state.slug.toLowerCase(),
              content : this.state.content,
              category : this.state.category || false,
              photos: this.state.photos,
              thumbnail: this.state.thumbnail,
              typeContent : this.state.typeContent,
              status: this.state.status,
              createAt: this.state.createAt,
              updateAt: TOOLS.getDate(),
            };
            firebase.database().ref('apps/'+ this.state.appID +'/pages/'+pageID)
              .update(dataUpdate, () => {
                this.setState({
                  isLoading : false,
                  pageID : pageID,
                  photos: [url],
                  thumbnail: [url],
                  status : 'Publicado',
                  status_response : true,
                  message_response : 'Foto detaque gravada com sucesso! {autosave}'
                });
                console.clear();
              });

            if(this.state.photos.length === files.length){
              // console.log('if this.state.photos.length -> ',this.state.photos.length);
              this.setState({isLoading:false});
            }else{
              // console.log('this.state.photos.length < -> ',this.state.photos.length);
            }
          }).catch((error) => {
            console.log(error);
          });
        });

        // let collectionPhotos = this.state.photos;
        // console.log('collectionPhotos -> ', collectionPhotos);
        console.log('uploadTask -> ', uploadTask);
      });
    }
  };//runUploadFiles();



  /*
   *	 RENDER FUNCTIONS - ACTIONS
   *
   *  Funções que renderizam a view do component,
   *  de acordo a ACTION de this.props.action;
   *  Todas as "ACTIONS Functions" definem document.title:
   *  Ex: document.title = APP_CONFIG.PROJECT_NAME + ' | Page Name';
   *  { actionNull - actionNew - actionEdit - actionDelete }
   */
  actionNull(){
    /*
     *  Responsável por renderizar a table de Páginas
     *  if( this.state.pages.length > 0 )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Conteúdo';
    if( !this.state.isLoading ){
      if( this.state.pages.length > 0 ){
        const renderList = () => {
          // eslint-disable-next-line
          return TOOLS.orderbyPostsDate(this.state.pages, 'desc').map((page) => {
            if(this.state.filter === 'Páginas'){
              if(page.typeContent === 'Page'){
                return (
                  <tr key={page.pageID}>
                    <td style={{width:80}}>
                      <Link to={'/dashboard/pages/edit/'+page.pageID} className={'btn btn-xs btn-primary'}>
                        <span className={'fa fa-pencil'}></span>
                      </Link>
                      <Link to={'/dashboard/pages/delete/'+page.pageID} onClick={() => {this.setState({showModal:true,title:page.title,pageID:page.pageID})}} className={'btn btn-xs btn-danger'}>
                        <span className={'fa fa-trash'}></span>
                      </Link>
                    </td>
                    <td style={{width:60}}><small>{page.pageID}</small></td>
                    <td>
                      <h4>
                        <Link to={'/dashboard/pages/edit/'+page.pageID}>
                          {page.title}
                          <span className={'fa fa-pencil'}></span>
                        </Link>
                      </h4>
                      <small>TIPO: <small className={'label label-xs label-default no-radius'}>
                        {page.typeContent}
                      </small></small>
                      <small>CRIADO EM: <small className={'label label-xs label-success no-radius'}>
                        {moment(page.createAt).format('DD/MM/YYYY - hh:mm:ss')}
                      </small> ALTERADO EM: <small className={'label label-xs label-success no-radius'}>
                        {moment(page.updateAt).format('DD/MM/YYYY - hh:mm:ss')}
                      </small></small>

                      <small>RESUMO: {TOOLS.stripAllTags(page.content).slice(0,70)+'..'}</small>
                    </td>
                    <td><small className={'label label-md label-primary no-radius'}>{page.category ? page.category : 'SEM CATEGORIA'}</small></td>
                    <td className={'text-center'}>{page.status === 'Publicado' ? <small className={'label label-xs label-success no-radius'}>{page.status}</small> : <small>{page.status}</small> }</td>
                  </tr>
                );
              }
            }else if(this.state.filter === 'Posts'){
              if(page.typeContent === 'Post'){
                if(this.state.filterCategory !== 'Selecione uma categoria' && this.state.filterCategory === page.category){
                  return (
                    <tr key={page.pageID}>
                      <td style={{width:80}}>
                        <Link to={'/dashboard/pages/edit/'+page.pageID} className={'btn btn-xs btn-primary'}>
                          <span className={'fa fa-pencil'}></span>
                        </Link>
                        <Link to={'/dashboard/pages/delete/'+page.pageID} onClick={() => {this.setState({showModal:true,title:page.title,pageID:page.pageID})}} className={'btn btn-xs btn-danger'}>
                          <span className={'fa fa-trash'}></span>
                        </Link>
                      </td>
                      <td style={{width:60}}><small>{page.pageID}</small></td>
                      <td>
                        <h4>
                          <Link to={'/dashboard/pages/edit/'+page.pageID}>
                            {page.title}
                            <span className={'fa fa-pencil'}></span>
                          </Link>
                        </h4>
                        <small>TIPO: <small className={'label label-xs label-default no-radius'}>
                          {page.typeContent}
                        </small></small>
                        <small>CRIADO EM: <small className={'label label-xs label-success no-radius'}>
                          {moment(page.createAt).format('DD/MM/YYYY - hh:mm:ss')}
                        </small> ALTERADO EM: <small className={'label label-xs label-success no-radius'}>
                          {moment(page.updateAt).format('DD/MM/YYYY - hh:mm:ss')}
                        </small></small>

                        <small>RESUMO: {TOOLS.stripAllTags(page.content).slice(0,70)+'..'}</small>
                      </td>
                      <td><small className={'label label-md label-primary no-radius'}>{page.category ? page.category : 'SEM CATEGORIA'}</small></td>
                      <td className={'text-center'}>{page.status === 'Publicado' ? <small className={'label label-xs label-success no-radius'}>{page.status}</small> : <small>{page.status}</small> }</td>
                    </tr>
                  );
                }else{
                  return (
                    <tr key={page.pageID}>
                      <td style={{width:80}}>
                        <Link to={'/dashboard/pages/edit/'+page.pageID} className={'btn btn-xs btn-primary'}>
                          <span className={'fa fa-pencil'}></span>
                        </Link>
                        <Link to={'/dashboard/pages/delete/'+page.pageID} onClick={() => {this.setState({showModal:true,title:page.title,pageID:page.pageID})}} className={'btn btn-xs btn-danger'}>
                          <span className={'fa fa-trash'}></span>
                        </Link>
                      </td>
                      <td style={{width:60}}><small>{page.pageID}</small></td>
                      <td>
                        <h4>
                          <Link to={'/dashboard/pages/edit/'+page.pageID}>
                            {page.title}
                            <span className={'fa fa-pencil'}></span>
                          </Link>
                        </h4>
                        <small>TIPO: <small className={'label label-xs label-default no-radius'}>
                          {page.typeContent}
                        </small></small>
                        <small>CRIADO EM: <small className={'label label-xs label-success no-radius'}>
                          {moment(page.createAt).format('DD/MM/YYYY - hh:mm:ss')}
                        </small> ALTERADO EM: <small className={'label label-xs label-success no-radius'}>
                          {moment(page.updateAt).format('DD/MM/YYYY - hh:mm:ss')}
                        </small></small>

                        <small>RESUMO: {TOOLS.stripAllTags(page.content).slice(0,70)+'..'}</small>
                      </td>
                      <td><small className={'label label-md label-primary no-radius'}>{page.category ? page.category : 'SEM CATEGORIA'}</small></td>
                      <td className={'text-center'}>{page.status === 'Publicado' ? <small className={'label label-xs label-success no-radius'}>{page.status}</small> : <small>{page.status}</small> }</td>
                    </tr>
                  );
                }
              }
            }else{
              return (
                <tr key={page.pageID}>
                  <td style={{width:80}}>
                    <Link to={'/dashboard/pages/edit/'+page.pageID} className={'btn btn-xs btn-primary'}>
                      <span className={'fa fa-pencil'}></span>
                    </Link>
                    <Link to={'/dashboard/pages/delete/'+page.pageID} onClick={() => {this.setState({showModal:true,title:page.title,pageID:page.pageID})}} className={'btn btn-xs btn-danger'}>
                      <span className={'fa fa-trash'}></span>
                    </Link>
                  </td>
                  <td style={{width:60}}><small>{page.pageID}</small></td>
                  <td>
                    <h4>
                      <Link to={'/dashboard/pages/edit/'+page.pageID}>
                        {page.title}
                        <span className={'fa fa-pencil'}></span>
                      </Link>
                    </h4>
                    <small>TIPO: <small className={'label label-xs label-default no-radius'}>
                      {page.typeContent}
                    </small></small>
                    <small>CRIADO EM: <small className={'label label-xs label-success no-radius'}>
                      {moment(page.createAt).format('DD/MM/YYYY - hh:mm:ss')}
                    </small> ALTERADO EM: <small className={'label label-xs label-success no-radius'}>
                      {moment(page.updateAt).format('DD/MM/YYYY - hh:mm:ss')}
                    </small></small>

                    <small>RESUMO: {TOOLS.stripAllTags(page.content).slice(0,70)+'..'}</small>
                  </td>
                  <td><small className={'label label-md label-primary no-radius'}>{page.category ? page.category : 'SEM CATEGORIA'}</small></td>
                  <td className={'text-center'}>{page.status === 'Publicado' ? <small className={'label label-xs label-success no-radius'}>{page.status}</small> : <small>{page.status}</small> }</td>
                </tr>
              );
            }
          });
        }

        return (
          <div className={'table-responsive'}>
            <Col xs={12} md={12} className={'no-padding'}>
              {this.state.filter === 'Posts' ?
                <Col xs={12} md={6} className={'no-padding'}>
                  <Col xs={12} md={2} className={'no-padding pull-left'}>
                    <DropdownButton
                      bsStyle={'default'}
                      title={this.state.filter}
                      id={'filter'}
                      className={'no-radius'}>
                        <MenuItem onClick={this.onSelectFilter}>Páginas</MenuItem>
                        <MenuItem onClick={this.onSelectFilter}>Posts</MenuItem>
                    </DropdownButton>
                  </Col>
                  <Col xs={12} md={5} className={'no-padding pull-left'}>
                    {this.onSelectFilterCategoryList()}
                  </Col>
                </Col>
              :
                <Col xs={12} md={2} className={'no-padding'}>
                  <DropdownButton
                    bsStyle={'default'}
                    title={this.state.filter}
                    id={'filter'}
                    className={'no-radius pull-left'}>
                      <MenuItem onClick={this.onSelectFilter}>Páginas</MenuItem>
                      <MenuItem onClick={this.onSelectFilter}>Posts</MenuItem>
                  </DropdownButton>
                </Col>
              }
              <Col xs={12} md={6} className={'no-padding'}>

              </Col>
            </Col>
            <table className={'table table-hover'}>
              <thead>
                <tr>
                  <th></th>
                  <th><small>ID</small></th>
                  <th><small>TÍTULO</small></th>
                  <th><small>CATEGORIA</small></th>
                  <th className={'text-center'}><small>STATUS</small></th>
                </tr>
              </thead>
              <tbody>
                { renderList() }
              </tbody>
            </table>
          </div>
        );
      }else{
        return (
          <Col xs={12} md={12} style={{textAlign:'center',padding:0}}>
            <div className="spinner">
              <div className="double-bounce1"></div>
              <div className="double-bounce2"></div>
            </div>
          </Col>
        );
      }
    }else{
      return (
        <div className={'table-responsive'}>
          <table className={'table table-hover'}>
            <thead>
              <tr>
                <th>ID</th>
                <th>TÍTULO</th>
                <th>TIPO</th>
                <th>CATEGORIA</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td className={'text-center'}>{'Nenhum registro foi encontrado... '}</td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      );
    }
  };//actionNull();

  actionNew(){
    /*
     *  Responsável por renderizar o Form para inserção de Páginas
     *  if( this.props.action === 'new' )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Adicionar Conteúdo';

    return (
      <div>
        <Col xs={12} md={12} className={'no-padding'}>
          <form onChange={this.onChangeForm} onSubmit={this.onSubmitForm}>
            <Col xs={12} md={8} className={'no-padding'}>
              <div className={'input-group col-md-12'}>
                <label>Título</label>
                <input
                  type={'hidden'}
                  ref={'pageID'}
                  value={TOOLS.uniqueID()}
                  />
                  <input
                    type={'hidden'}
                    ref={'status'}
                    value={'Publicado'}
                    />
                <input
                  className={'form-control required'}
                  onBlur={TOOLS.requiredInput}
                  onChange={this._onChangeTitle}
                  type="text"
                  ref="title"
                  autoFocus
                  placeholder="Digite o título aqui" />
                  <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
                    <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Slug:</span> <span ref={'slug'} className={ this.state.slug ? 'label label-success' : 'label label-default'}>{this.state.slug ? this.state.slug : 'Aguardando um título..'}</span>
                  </label>
              </div>

              <div className={'input-group col-md-12'} style={{marginTop:20}}>
                <ReactQuill
                  theme={'snow'}
                  ref={'content'}
                  value={this.state.content}
                  onChange={this.onChangeContent}
                  />
              </div>
            </Col>
            <Col xs={12} md={4} className={'no-paddingRight'}>
              {this.statusInfo()}
              {this.renderType()}
              <Button
                onClick={this.onSubmitForm}
                className="btn btn-success pull-right no-radius"
                style={{marginTop:30}}
                >Publicar
              </Button>
              <Link
                to={'/dashboard/pages/'}
                className="btn btn-danger pull-right no-radius"
                style={{marginTop:30}}
                >Cancelar
              </Link>
            </Col>
          </form>
        </Col>
      </div>
    );
  };//actionNew();

  actionEdit(){
    /*
     *  Responsável por renderizar o Form para alteração de Páginas
     *  if( this.props.action === 'edit' )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Editar Conteúdo';

    if(this.props.action === 'edit'){
      console.log('action: ',this.props.action);
      if(this.props.pageID){
        return (
          <div>
            <Col xs={12} md={12} className={'no-padding'}>
              <MasterForm onLoadForm={this.onLoadForm} onChange={this.onChangeForm} onSubmit={this.onUpdateForm}>
                <Col xs={12} md={8} className={'no-padding'}>
                  <div className={'input-group col-md-12'}>
                    <label>Título</label>
                    <FormControl
                      type={'hidden'}
                      ref={'pageID'}
                      value={this.props.pageID}
                      />
                    <FormControl
                      className={'form-control'}
                      onBlur={TOOLS.requiredInput}
                      onChange={this._onChangeTitle}
                      type="text"
                      ref={'title'}
                      value={this.state.title}
                      placeholder="Digite o título aqui" />
                      <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
                        <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Slug:</span> <span ref={'slug'} className={ this.state.slug ? 'label label-success' : 'label label-default'}>{this.state.slug ? this.state.slug : 'Aguardando um título..'}</span>
                      </label>
                  </div>

                  <div className={'input-group col-md-12'} style={{marginTop:20}}>
                    <ReactQuill
                      theme={'snow'}
                      ref={'content'}
                      value={this.state.content}
                      onChange={this.onChangeContent}
                      />
                  </div>
                </Col>
                <Col xs={12} md={4} className={'no-paddingRight'}>
                  {this.statusInfo()}
                  {this.renderType()}
                  <Button
                    onClick={this.onUpdateForm}
                    className="btn btn-success pull-right no-radius"
                    style={{marginTop:30}}
                    >Atualizar
                  </Button>
                  <Link
                    to={'/dashboard/pages/'}
                    className="btn btn-danger pull-right no-radius"
                    style={{marginTop:30}}
                    >Cancelar
                  </Link>
                </Col>
              </MasterForm>
            </Col>
          </div>
        );
      }else{
        return (<h1>Ops.. ocorreu uma falha..</h1>);
      }
      // console.log('pageID: ',this.props.pageID);
    }
  };//actionEdit();

  actionDelete(){
    /*
     *  Responsável por deletar páginas
     *  if( this.props.action === 'delete' )
     */
     if(this.props.action === 'delete'){
       if(this.props.pageID){
         if(!this.state.confirmDelete){
           return (
             <div>
               {this.actionNull()}
               {this.renderModal()}
             </div>
           );
         }else{
           console.log('confirmDelete!');
           return (
             <div>
               {this.actionNull()}
             </div>
           );
         }
       }else{
         return (
           <h1>actionDelete #OFF</h1>
         );
       }
     }
  };//actionDelete();

  renderAction(){
    /*
     *  Responsável por verificar e chamar ACTIONS.
     *  Ex: if( this.props.action === 'new' ) this.actionNew();
     */
    if( this.props.action === 'new' )
      return this.actionNew();

    if(this.props.action === 'edit')
      return this.actionEdit();

    if(this.props.action === 'delete')
      return this.actionDelete();


    if( !this.props.action )
      return (<div>{this.actionNull()}</div>);
  };//renderAction();

  render(){
    return (
      <div>
        {this.checkStatusResponse()}
        {
          this.renderAction()
        }
      </div>
    );
  }
}
