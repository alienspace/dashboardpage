import React, { Component } from 'react';
import {
  Col, Button, DropdownButton, MenuItem,
  Alert,
} from 'react-bootstrap';
import firebase from 'firebase';
import slug from 'slug';
import TinyMCE from 'react-tinymce';

/** COMPONENTS CUSTOM **/
import { APP_CONFIG } from './../../boot.config';
import { Router, Link, } from 'react-router';
export class Pages extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading : true,
      action : this.props.action,
      pages : [],

      uid : this.props.currentUser.uid,
      pageID : null,
      title : null,
      slug : null,
      content : null,
      typeContent : 'Page',
      category : null,
      status : null,

      status_response : null,
      message_response : null
    }

    this.checkStatusResponse = this.checkStatusResponse.bind(this);
    this.checkStates = this.checkStates.bind(this);
    this.getPages = this.getPages.bind(this);
    this.requiredInput = this.requiredInput.bind(this);

    /*** FUNCTIONS AUXILIARES DO FORM ***/
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onUpdateForm = this.onUpdateForm.bind(this);
    this.onChangeForm = this.onChangeForm.bind(this);
    this.onChangeTiny = this.onChangeTiny.bind(this);
    this.renderType = this.renderType.bind(this);
    this.onSelectCategory = this.onSelectCategory.bind(this);
    this.onSelectTypeContent = this.onSelectTypeContent.bind(this);
    this.statusInfo = this.statusInfo.bind(this);

    /*** RENDER FUNCTIONS - ACTIONS ***/
    this.actionNull = this.actionNull.bind(this);
    this.actionNew = this.actionNew.bind(this);
    this.renderAction = this.renderAction.bind(this);

    if(this.props.pageID){
      this.getPages(this.props.pageID);
    }
  }

  componentWillMount(){
    // this.getPages();
    if(this.props.action === 'edit' && this.props.params.pageID){
      this.setState({ pageID : this.props.params.pageID });
    }
  }

  componentDidMount(){
    console.log(this.state.uid);
    let _self=this;
    _self.allPages = firebase.database().ref('pages/');
    _self.pages = this.state.pages;

    _self.allPages.on('child_added', (data) => {
      _self.pages.push(data.val());
      if(data){
        if(_self.pages.length > 0 )
          _self.setState({ isLoading : false });

        _self.setState({ pages : _self.pages });
      }
      console.log('pages: ', {qtd: _self.pages.length, all: _self.pages});
    });
  };//componentDidMount()

  componentDidUpdate(){
    // console.log('componentDidUpdate');
  };//componentDidUpdate()

  checkStatusResponse(){
    const closeAlert = (e) => {
      if(e)
        e.preventDefault();
      this.setState({ status_response : null, message_response : null});
    };//closeAlert(e);
    if(this.state.status_response === true){
      setTimeout(() => {
        closeAlert();
      },4000);
      return (<Alert bsStyle={'success'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    if(this.state.status_response === false){
      setTimeout(() => {
        closeAlert();
      },4000);
      if(this.state.status === 'Rascunho'){
        return (<Alert className={'default'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }else if(this.state.status === 'requiredInput'){
        return (<Alert className={'warning'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }
      return (<Alert bsStyle={'danger'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    return null;
  };//checkStatusResponse();

  checkStates(){
    if(this.state.pageID)
      this.setState({ pageID : this.state.pageID })

    console.log('checkStates exit();');
  };//checkStates();

  getPages(pageID=null){
    // const page = pageID;
    if( pageID ){
      // eslint-disable-next-line
      return this.state.pages.map((page) => {
        if(page.pageID === pageID){
          this.setState({
            pageID : page.pageID,
            title : page.title,
            slug : page.slug,
            content : page.content,
            typeContent : page.typeContent,
            category : page.category,
            status: page.status
          });
        }else{
          return false;
        }
      });
      // firebase.database().ref('pages/'+pageID).once('value')
      //   .then((res) => {
      //     console.log('getPage 1 res ',res.val().name);
      //   })
      //   .catch((err) => {
      //     console.log('getPage 1 err',err);
      //   })
    }
    this.setState({ isLoading : false });
  };//getPages();

  requiredInput(e){
    if(!e.target.value || e.target.value === ''){
      e.target.classList.add('required');
    }else{
      e.target.classList.remove('required');
    }
    return false;
  };//requiredInput(e);




  /*** FUNCTIONS AUXILIARES DO FORM ***/
  onSubmitForm(e) {
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const pageID = this.refs.pageID.value;
      const dataInsert = {
        uid : this.state.uid,
        pageID : pageID,
        title : this.state.title,
        slug : this.state.slug,
        content : this.state.content,
        typeContent : this.state.typeContent,
        category : this.state.category,
        status: this.state.status
      };
      firebase.database().ref('pages/'+pageID)
        .set(dataInsert);
      this.setState({
        isLoading : false,
        status : 'Publicado',
        status_response : true,
        message_response : '{ ' + this.state.title + ' } gravado com sucesso!'
      });
      Router.push('/pages/edit/'+this.state.pages.length + 1);
    }
    return false;
  };//onSubmitForm(e);
  onUpdateForm(e){
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const pageID = this.props.params.pageID;
      const dataUpdate = {
        uid : this.state.uid,
        pageID : pageID,
        title : this.state.title,
        slug : this.state.slug,
        content : this.state.content,
        typeContent : this.state.typeContent,
        category : this.state.category,
        status: this.state.status
      };
      firebase.database().ref('pages/'+pageID)
        .update(dataUpdate);
      this.setState({
        isLoading : false,
        pageID : pageID,
        status : 'Publicado',
        status_response : true,
        message_response : '{ ' + this.state.title + ' } gravado com sucesso!'
      });
    }
    return false;
  };//onUpdateForm(e);
  onChangeForm(e){
    if(e)
      this.setState({
        pageID : this.refs.pageID.value,
        title : this.refs.title.value,
        slug: slug(this.refs.title.value),
        content : this.state.content,
        typeContent : this.state.typeContent,
        category : this.state.category,
        status: this.state.status
      });
  };//onChangeForm(e);
  onChangeTiny(e){
    if(e)
      this.setState({ content : e.target.getContent() });
  };//onChangeTiny(e);
  renderType(){
    if(this.state.typeContent === 'Post'){
      return (
        <div>
          <label>Tipo de Conteúdo</label>
          <DropdownButton id={'typeContent'} className={'col-xs-12 col-md-12'} title={this.state.typeContent}>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectTypeContent}>Page</MenuItem>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectTypeContent}>Post</MenuItem>
          </DropdownButton>
          <DropdownButton id={'categoryList'} className={'col-xs-12 col-md-12'} style={{marginTop:20}} title={this.state.category}>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectCategory}>Default</MenuItem>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectCategory}>Categoria 1</MenuItem>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectCategory}>Categoria 2</MenuItem>
          </DropdownButton>
        </div>
      );
    }else{
      return (
        <div>
          <label>Tipo de Conteúdo</label>
          <DropdownButton id={'typeContent'} className={'col-xs-12 col-md-12'} title={this.state.typeContent}>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectTypeContent}>Page</MenuItem>
            <MenuItem className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectTypeContent}>Post</MenuItem>
          </DropdownButton>
        </div>
      );
    }
  };//renderType();
  onSelectCategory(e){
    if(e && e.target.innerHTML !== '')
      this.setState({ category : e.target.innerHTML });
  };//onSelectCategory();
  onSelectTypeContent(e){
    if(e && e.target.innerHTML === 'Page')
      this.setState({ typeContent : e.target.innerHTML, category : null });

    if(e && e.target.innerHTML === 'Post')
      this.setState({ typeContent : e.target.innerHTML, category : 'Selecione a Categoria' });
  };//onSelectTypeContent(e);
  statusInfo(){
    if( this.state.status_response === false ){
      if(this.state.status === 'Rascunho')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'slug'} className={ 'label label-default' }>{this.state.status || 'Aguardando publicação..'}</span>
          </label>
        );

      if(this.state.status === 'requiredInput')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'slug'} className={ 'label label-default' }>{'Rascunho'}</span>
          </label>
        );
    }else if(this.state.status_response === true){
      return (
        <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
          <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'slug'} className={ 'label label-success' }>{'Rascunho'}</span>
        </label>
      );
    }else{
      return (
        <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
          <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'slug'} className={ 'label label-default' }>{'Aguardando um título'}</span>
        </label>
      );
    }
  };//statusInfo();



  /*
   *	 RENDER FUNCTIONS - ACTIONS
   *
   *  Funções que renderizam a view do component,
   *  de acordo a ACTION de this.props.action;
   *  Todas as "ACTIONS Functions" definem document.title:
   *  Ex: document.title = APP_CONFIG.PROJECT_NAME + ' | Page Name';
   *  { actionNull - actionNew - actionEdit - actionDelete }
   */
  actionNull(){
    /*
     *  Responsável por renderizar a table de Páginas
     *  if( this.state.pages.length > 0 )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Conteúdo';
    if( !this.state.isLoading ){
      if( this.state.pages.length > 0 ){
        const renderList = () => {
          return this.state.pages.map((page) => {
            return (
              <tr key={page.pageID}>
                <td><Link to={'/pages/edit/'+page.pageID} className={'btn btn-xs btn-primary'}><span className={'fa fa-pencil'}></span></Link></td>
                <td>{page.title}</td>
                <td>{page.typeContent}</td>
                <td>{page.category}</td>
              </tr>
            );
          });
        }

        return (
          <div className={'table-responsive'}>
            <table className={'table table-hover'}>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>TÍTULO</th>
                  <th>TIPO</th>
                  <th>CATEGORIA</th>
                </tr>
              </thead>
              <tbody>
                { renderList() }
              </tbody>
            </table>
          </div>
        );
      }else{
        return (
          <div className={'table-responsive'}>
            <table className={'table table-hover'}>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>TÍTULO</th>
                  <th>TIPO</th>
                  <th>CATEGORIA</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td className={'text-center'}>{'Nenhum registro foi encontrado... '}</td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>
        );
      }
    }else{
      return (
        <Col xs={12} md={12} style={{textAlign:'center',padding:0}}>
          <div className="spinner">
            <div className="double-bounce1"></div>
            <div className="double-bounce2"></div>
          </div>
        </Col>
      );
    }
  };//actionNull();

  actionNew(){
    /*
     *  Responsável por renderizar o Form para inserção de Páginas
     *  if( this.props.action === 'new' )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Adicionar Conteúdo';

    return (
      <div>
        <Col xs={12} md={12} className={'no-padding'}>
          <form onChange={this.onChangeForm} onSubmit={this.onSubmitForm}>
            <Col xs={12} md={8} className={'no-padding'}>
              <div className={'input-group col-md-12'}>
                <label>Título</label>
                <input
                  type={'hidden'}
                  ref={'pageID'}
                  value={this.state.pages.length + 1}
                  />
                <input
                  className={'form-control required'}
                  onBlur={this.requiredInput}
                  type="text"
                  ref="title"
                  placeholder="Digite o título aqui" />
                  <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
                    <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Slug:</span> <span ref={'slug'} className={ this.state.slug ? 'label label-success' : 'label label-default'}>{this.state.slug ? this.state.slug : 'Aguardando um título..'}</span>
                  </label>
              </div>

              <div className={'input-group col-md-12'} style={{marginTop:20}}>
                <TinyMCE
                  content={this.state.content}
                  config={{
                    skin: 'lightgray',
                    statusbar: false,
                    menubar: false,
                    height: 280,
                    plugins: 'link image code',
                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                  }}
                  onChange={this.onChangeTiny}
                />
              </div>
            </Col>
            <Col xs={12} md={4} className={'no-paddingRight'}>
              {this.statusInfo}
              {this.renderType()}
              <Button
                onClick={this.onSubmitForm}
                className="btn btn-success pull-right no-radius"
                style={{marginTop:30}}
                >Publicar
              </Button>
              <Link
                to={'/pages/'}
                className="btn btn-danger pull-right no-radius"
                style={{marginTop:30}}
                >Cancelar
              </Link>
            </Col>
          </form>
        </Col>
      </div>
    );
  };//actionNew();

  actionEdit(){
    /*
     *  Responsável por renderizar o Form para alteração de Páginas
     *  if( this.props.action === 'edit' )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Editar Conteúdo';

    if(this.props.action === 'edit'){
      console.log('action: ',this.props.action);
      if(this.props.pageID){
        console.log('pageID: ',this.props.pageID);
        return (
          <div>
            <Col xs={12} md={12} className={'no-padding'}>
              <form onChange={this.onChangeForm} onSubmit={this.onUpdateForm}>
                <Col xs={12} md={8} className={'no-padding'}>
                  <div className={'input-group col-md-12'}>
                    <label>Título</label>
                    <input
                      type={'hidden'}
                      ref={'pageID'}
                      value={this.props.pageID}
                      />
                    <input
                      className={'form-control'}
                      onBlur={this.requiredInput}
                      type="text"
                      ref={'title'}
                      value={this.state.title || ''}
                      placeholder="Digite o título aqui" />
                      <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
                        <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Slug:</span> <span ref={'slug'} className={ this.state.slug ? 'label label-success' : 'label label-default'}>{this.state.slug ? this.state.slug : 'Aguardando um título..'}</span>
                      </label>
                  </div>

                  <div className={'input-group col-md-12'} style={{marginTop:20}}>
                    <TinyMCE
                      content={this.state.content}
                      config={{
                        skin: 'lightgray',
                        statusbar: false,
                        menubar: false,
                        height: 280,
                        plugins: 'link image code',
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                      }}
                      onChange={this.onChangeTiny}
                    />
                  </div>
                </Col>
                <Col xs={12} md={4} className={'no-paddingRight'}>
                  {this.statusInfo}
                  {this.renderType}
                  <Button
                    onClick={this.onUpdateForm}
                    className="btn btn-danger pull-right no-radius"
                    style={{marginTop:30}}
                    >Atualizar
                  </Button>
                  <Link
                    to={'/pages/'}
                    className="btn btn-danger pull-right no-radius"
                    style={{marginTop:30}}
                    >Cancelar
                  </Link>
                </Col>
              </form>
            </Col>
          </div>
        );
      }else{
        return (<h1>Ops.. ocorreu uma falha..</h1>);
      }
      // console.log('pageID: ',this.props.pageID);
    }
  };//actionEdit();

  renderAction(){
    /*
     *  Responsável por verificar e chamar ACTIONS.
     *  Ex: if( this.props.action === 'new' ) this.actionNew();
     */
    if( this.props.action === 'new' )
      return this.actionNew();

    if(this.props.action === 'edit'){
      return this.actionEdit();
    }

    if( !this.props.action )
      return (<div>{this.actionNull()}</div>);
  };//renderAction();

  render(){
    return (
      <div>
        {this.checkStatusResponse()}
        {
          this.renderAction()
        }
      </div>
    );
  }
}
